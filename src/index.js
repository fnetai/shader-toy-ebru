import ShaderToy from "@fnet/shader-toy";

import fshader from "./fshader.glsl";

export default async (args) => {
    await ShaderToy({ fshader, canvas: { backgroundColor: "#000" }, stats: true, ...args })
}