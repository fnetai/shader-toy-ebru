uniform vec3 iColor;       // Base color for the shader
uniform float iIteration;  // Number of iterations in the loop
uniform float iTimeScale;  // Time scaling factor

// Entry point for the fragment shader
void mainImage(out vec4 fragColor, in vec2 fragCoord) {
    // Normalize the fragment coordinates
    vec2 uv = (2.0 * fragCoord - iResolution.xy) / min(iResolution.x, iResolution.y);
    
    // Determine the time scaling factor, default to 1.0 if not set
    float timeScale = (iTimeScale == 0.0) ? 1.0 : iTimeScale;
    
    // Apply the time scaling factor to iTime
    float scaledTime = iTime / 12. * timeScale;
    
    // Determine the base color, default to a specific color if not set
    vec3 baseColor = (iColor == vec3(0.0)) ? vec3(0.451, 0.0471, 0.0471) : iColor;
    
    // Determine the number of iterations, default to 12 if not set
    float iteration = (iIteration == 0.0) ? 12.0 : iIteration;
    
    // Main loop to generate the wavy pattern
    for(float i = 1.0; i < iteration; i++) {
        uv.x += 0.6 / i * cos(i * 2.5 * uv.y + scaledTime);
        uv.y += 0.6 / i * cos(i * 1.5 * uv.x + scaledTime);
    }
    
    // Calculate the final fragment color
    fragColor = vec4(baseColor * abs(sin(scaledTime - uv.x - uv.y)), 1.0);
}
